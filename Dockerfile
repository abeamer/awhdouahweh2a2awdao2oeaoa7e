FROM opensuse/leap:15.5

RUN zypper update -y \
    && zypper install -y vim curl tcpdump
