### **1. Users Name Output and Crontab Entries**

#### List and Format Users in /etc/passwd
```
#!/usr/bin/env bash
# Script to output the username and home directory of
# every user on a UNIX system, with a ":" delimiter added.

# This script was tested and run on an OpenSUSE Leap 15.5 container
# on RancherOS using GNU bash, version 4.4.23(1)-release (aarch64-suse-linux-gnu)
# and was written to conform with the with Google Shell Style Guide:
# https://google.github.io/styleguide/shellguide.html

# By default, most UNIX systems store users in /etc/passwd,
# and passwords in /etc/shadow, but lets add some flexibility;
# also, using variables makes inevitable refactoring easier

# Bash best practices dictate that variables should be all caps
USER_FILE=/etc/passwd

function main() {
  awk -F: '{print $1":"$6}' ${USER_FILE}
}

main "${@}"
```
#### Track User Changes
```
#!/usr/bin/env bash
# Script to take the output of the above and create an MD5 sum from it,
# placing these values in /var/log/current_users.

# This script was tested and run on an OpenSUSE Leap 15.5 container
# on RancherOS using GNU bash, version 4.4.23(1)-release (aarch64-suse-linux-gnu)
# and was written to conform with the with Google Shell Style Guide:
# https://google.github.io/styleguide/shellguide.html

function main() {
  # Output our time in ISO8601 with UTC time
  ISO_8601_DATE_TIME=$(date -u +%F"T"%R:%S)
  # Our requisite files
  CURRENT_USERS=/var/log/current_users
  USER_CHANGES=/var/log/user_changes
  # Read through the input from the first script and compute an MD5 sum
  USERS_HASH=$(md5sum "${@}" | awk '{print $1}')
  # Does /var/log/current_users exist?
  if [[ ! -e "${CURRENT_USERS}" ]]; then
    # Output the user hash into /var/log/current_users,
    # as this is the first time this script has run.
    echo "${USERS_HASH}" >> "${CURRENT_USERS}"
  else
    CURRENT_USER_HASH=$(cat "${CURRENT_USERS}")
    if [[ "${CURRENT_USER_HASH}" != "${USERS_HASH}" ]]; then
    # The users data has changed and must be updated in /var/log/user_changes
      echo "${ISO_8601_DATE_TIME} changes occurred" >> "${USER_CHANGES}"
      # Replace the old hash in /var/log/current_users
      echo "${USERS_HASH}" > "${CURRENT_USERS}"
    fi
  fi
}

main "${@}"
```
### Output from /etc/crontab

Assumes that the scripts are stored in `/root/scripts`
```
0 * * * * root /root/scripts/users.sh | /root/scripts/user_hash.sh
```

## **2. User complains of slow access times to web app**

### Assuming there is no external monitoring solution e.g. DataDog, Nagios etc.

**Step 1.** Log onto the host and run `w` to check for other logged in users.
Rule out that any other engineer(s) or developer(s) are performing any actions that may slow the
system down. This could range from expensive database queries to backups to configuration changes.
If other people are logged on, reach out to them and inquire to their actions.

**Step 2.** Run `uptime`, `top` and `free [-h|-m]` to get immediate insight into overall
system utilization with regards to RAM and CPU. `uptime` will show overall system load, `top` will show a
running list of system processes, generally sorted by CPU utilization, `free [-h|-m]` will report system RAM usage,
where `-h` reports memory usage in human readable values and `-m` shows memory usage in megabytes. If there are processes
that are consistently consuming all CPU and/or memory then this may indicate that it is time to vertically scale the host.

**Step 3.** Test if there are DNS issues. Run `dig $APPLICATION_FQDN`. Does this resolve
slowly? DNS is a common culprit for applications running slowly. If the DNS record for
the host has recently changed this may also be an issue, as the change may not have
fully propagated.

**Step 4.** Check if there are an excessive number of connections to the host with `ss -t4n`.
Web servers, particularly Apache `httpd` often have difficulty scaling beyond a certain
threshold of connections. Excessive stale connections or an especially large number of
waiting connections may be indications of possible DDOS. Application servers such as Gunicorn or Puma
need to be scaled with additional worker processes per core to increase performance. Check that the
number of workers matches the thread/core count on the server.

**Step 5.** Have there been any recent changes to the application? Check CI/CD pipelines and the application's configuration management
code e.g. Ansible, SaltStack, Puppet etc for changes to the configuration. Commonly, regressions may
not be discovered until deployed into production with large scale user activity.

**Step 6.** Does the host have an asymetric route? Misconfigured load balancers such as an
F5 ADC can introduce asymetric routes, which can slow or degrade performance. `traceroute`
is useful in this case. If there is a loadbalancer or infrastructure team, it may be helpful
to contact them.

**Step 7.** Are there database connection problems? Depending on the application, the connection
timeout for application and databases may need to be adjusted. Jira is an example of a web app
that has very long database connections. If the database has short/medium connection timeouts
this can negatively impact performance. Database connection poolers such as `pgbouncer` may also help
with connection reuse.

**Step 8.** How is the database health? Has the database grown rapidly? Are the queries still
efficient? SQL queries that were performant at 10 or 100 uers can become untenable at 1000
or 10,000 users. Were the database tables and schemas well developed? It is possible that the
DBA configured the database with an excessive number of tables and/or required numerous `JOIN`
operations. This can also rapidly impact performance.

**Step 9.** Analyze network traffic. `tcpdump` is a highly usefully utility for checking network
traffic on a host. Run `tcpdump port 443 or port 80`. Bearing in mind that size is relative,
are there a large number of connections, or are there numerous frequent connections from a handful
of servers? The latter may be indicative of a DDOS.

**Step 10.** The above can be considered house cleaning; if at this point there are no
indications that the problem lies in the host, update the trouble ticket, requesting additional
info from the user as to any changes they have made. Misconfigured settings on the user's end
easily degrade performance.

### **3. Git Commit Graph**

```
    1 (feature-branch)
   / \
A-B-C-D-E (head)
  (main)
```

The following `git` commands would create an equivalent graph to the above

1. `git add .` - Add the working directory to be tracked by Git

   `git commit -m "first commit"` - Make the first commit to the repository. This is commit `A`.

2. `git add -p .` - Add any files with untracked changes to be tracked. `-p`
                    allows selective patching of each hunk. This allows
                    review of each change.

   `git commit -m "second commit"` - Make the second commit to the `main` branch. This is commit `B`.

3. `git checkout -b feature-branch` - Create and checkout a new branch called `feature-branch`. This will be commit `1`.

   `git add -p .` - See above.

   `git commit -m "awesome feature"`

4. `git switch main` - Switch back to `main` branch.

   `git add -p .` - See above

   `git commit -m "third commit"` - This is commit `C`.

5. `git add -p .` - See above

   `git commit -m "third commit"` - Add the third commit to the `main` branch

6. `git checkout main` - Switch back to the `main` branch

   `git merge main feature-branch` - Join the `feature-branch` branch into the `main` branch.

7. `git add -p .` - See above

   `git commit -m "fourth commit"` - Add the third commit to the `main` branch. This will be commit `E`.

The above steps assume there are no merge conflicts when merging `feature-branch`.

### **4. Git Branching and Merging for Beginners**

#### Introduction

The intent of this course this course is to act as a basic primer to
using Git branches and merging. These two concepts are at the core of
Git's functionality and their power and flexibility are part of the
reason for Git's dominance as a version control tool.

This introduction presumes no previous experience with Git, only
basic Linux command line (Bash) knowledge.

#### Overview
Git is the de facto version control software for the majority
of software development. Unfortunately, it is NOT because
of its simplicity. Git commands have something of a reputation
for being arcane, or at the very list being unintuitive. This is a
reputation not unfairly earned. Fortunately, with a modicum of effort,
the basics of Git can be learned fairly quickly.

#### The Basics
Consider this scenario. Two developers are working on a piece of
software. Each of them are working on the same module with the then
current codebase copied onto their developer machines. Developer A adds in
a feature for user login timeouts and Developer B adds in a feature
that allows users to add a profile picture. They're both quite happy with
their changes and the testing on their local development environments shows
there should be no errors. Now they want to fully integrate those features into
the main codebase. This is where the problems arise.

Both developers were working on the same code, and they both have a different
set of changes. How can different edits to the same documents be handled?

Here is where Git can help. Git allows the seamless tracking of changes to a file or
set of files and allows the merging and branching thereof. Git will allow both developers to
make as many edits as they wish and to integrate each other's work, quickly and safely.

#### Creating a Git Repository
Before any work can begin, you must have a Git repository (repo).
In this case, we will start with a brand new repo.

First create a directory for your project to live in:

`mkdir ~/my_new_project`

And then cd in it:

`cd ~/my_new_project`

Now that you are in your project, lets create our Git repo:

`git init`

You should see the following output:

`Initialized empty Git repository in ${HOME}/my_new_project/.git/`

In the above case, substitue `${HOME}` for your home directory.

#### Our First File
Now we will create out first file. Open a text file in your repository
directory with the name: `hello.sh` and add the following:

```
#!/usr/bin/env bash

echo "Hello World"
```

The above is merely a simple "Hello World!" script that executes in Bash.

Next, lets make the script executable:

`chmod 755 hello.sh`

And run it:

`./hello.sh`

Which should produce the following:

`Hello World`

Finally, we are going to have Git track this file. When Git tracks a file it allows
us to keep track of changes or revert them, as well as other more complex tasks.

`git status`

You should see some of the following:

```
# On branch main
#
# Initial commit
#
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#	hello.sh
nothing added to commit but untracked files present (use "git add" to track)
```

Even before we have added a file, Git is already tracking the repo and can see that
we have added a file.

Now we will add the file to Git to be tracked

`git add hello.sh`

This will not product any output, so don't be alarmed. Now we will see
what Git has done with out file:

`git status`

```
# On branch main
#
# Initial commit
#
# Changes to be committed:
#   (use "git rm --cached <file>..." to unstage)
#
#	new file:   hello.sh
```

Git has 'staged' our changes. Theses changes have not been made final. If we wanted
to stop and 'unstage' our file we would simply type:

`git rm --cached hello.sh`

This unstages our file, stopping them from being tracked. Git will ignore these
files and they are not part of the repository. To have Git track them again,
run:

 `git add hello.sh`

 Now with Git tracking our file, we will add it to the repository:

 `git commit -m "My first commit"`

 This will finalize your changes. Note that the `-m` flag is for your message.
 It is always a good practice to leave a brief but clear message with each
 commit. That way anybody else who reads your logs can have a good sense of
 what changes have been made.

 Now when you run:

 `git status`

 You should see:

 ```
 # On branch main
nothing to commit, working directory clean
```

All of your changes have been stored in the Git repo. There is even a log of changes to
this repo that is viewable with:

`git log`

Along with showing the time and date of commits that were made and the message,
there is a also a commit hash at the top. This is a unique checksum of the
commit, and part of what allows Git to track changes.

### Branching Out
Now that we have a repository and a file that is being tracked, we are going to
jump in to the best feature of Git: branches.

But first, what is a branch? You can think of a branch as a separate workspace
for a Git repository. A branch is split off, has work performed in to it and then can be
easily merged back in. The branch allows a developer to work with code in their own
space without affecting other people's work.

Before we create a branch, lets look at what branches we have right now:

`git branch`

You should see the following:

`* main`

`main` is name of the default branch in Git. It is where everything starts.
The `*` indicates that we are currently on this branch.

Next we will create a new branch:

`git branch my_first_branch`

And we will look at our branches:

`git branch`

We should see the following:

```
* main
  my_first_branch
```

We have created a new branch, but not switched to it. To switch to a new
branch type the following:

`git checkout my_first_branch`

We should see the following:

`Switched to branch 'my_first_branch'`

At this point, other that switching to a new branch, nothing has changed.
Our `hello.sh` script is unchanged and nothing new has been added. We're going to change
that.

Open the `hello.sh` script and append the following line:

```
echo "How are you?"
```

And run the script:

```
./hello.sh

Hello World
How are you?
```

In this case, we can consider this a line a feature. Git is now tracking the change
we have made.

`git status`

With the output:

```
<snip>
#	modified:   hello.sh
<snip>
```

Now we will stage these changes:

`git add hello.sh`

And commit them to the `my_first_branch` branch:

`git commit -m "Added in a question after the greeting"`

Remember that you can see your commit message(s) with:

`git log`

#### Merging In
Up to this point we have created a script, tracked it with git, committed it to our repository,
created a new branch and added a feature in that branch. Now that we are finished with that, we
are going to 'merge' that feature into the main branch. Remember that branching and merging
is what allows multiple people to work on the same code and then bring those changes together,
all without stepping on each other's toes.

First, we will switch back to the `main` branch

`git checkout main`

Let's take a look at our script:

`./hello.sh`

And its output:

`Hello World`

What happened to the line we added back in our `my_first_branch` branch? These changes only exist
in that branch. What we do in one branch has no effect on the others. But now we want those
changes in the main, so we are going to merge them in:

`git merge main my_first_branch`

The above merges the changes from branch `my_first_branch` into the `main` branch.

Now if we run:

`./hello.sh`

We get:

`Hello World`
`How are you?`

#### Recap
We created a file, tracked it, branched our repository, changed the file by adding a feature
and lastly, merged those changes. This forms the core of the functionalithy of Git.
With these simple commands multiple people can collaboratively work on the same documents
and code without interfering and then combine them together.


#### Conclusion
While the above only scratches the surface of all of Git's features and functionality, it is hoped
this tutorial will allow you to get started with using Git in a productive manner, while also
revealing some of the benefits and uses in Git.

### **6. Book or Blog Review**

I recently finished reading the third edition `Terraform: Up and Running`, by Yevgeniy Brikman, whom is also the co-founder
of Gruntwork. Terraform is one of my favorite pieces of software in the industry and I use it in both personal and professional
projects. `Terraform: Up and Running` is both a fantastic introduction to Infrastucture-as-Code patterns and an excellent refresher
on the many features and capabilities of Terraform. Many IaC books are big on running toy projects or showing how the tool works in a
vacuum. `Terraform: Up and Running` actually delivers on teaching how to use Terraform in a production environment.

Additionally, I enjoy reading about [Literate Programming](https://zyedidia.github.io/literate/index.html), a concept I revist
every so often. Literate programming has real potential to be a highly effective tool in the world of DevOps and support engineering.
This stems from the focus on readability and code-as-a-story approach, making code that is actually readable to humans. Which itself
ties back into wisdom that was (ironically) taught to me from the O'Reilly's `Learning Perl` book, that anyone who writes code will
spend more time reading code than writing code. Therefore it is advised that one focuses on writing the most readable code and not
the most clever code.

### Works Cited

Too many `man` pages to count, but principally `bash(1)` and `git` et al

Additionally the following books have been absolutely paramount to my career and growth.

`The Linux Command Line` by William Shotts, No Starch Press

`The Linux Bible` by Christopher Negus, Wiley

[Pro Git](https://git-scm.com/book/en/v2), Apress but available under CC3.0
