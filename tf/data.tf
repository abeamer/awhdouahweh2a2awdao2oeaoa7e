data "aws_availability_zones" "azs" {
  state = "available"
}

data "aws_ami" "ubuntu_jammy_arm64" {
  most_recent = true
  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-arm64-*"]
  }
  filter {
    name = "architecture"
    values = ["arm64"]
  }
  owners = ["099720109477"]
}
