provider "aws" {
  region = "us-west-2"
  profile = "default"
}

locals {
  terraform_git_repo = "gitlab"
  terraform_base_path = replace(path.cwd, "/^.*?(${local.terraform_git_repo}\\/)/", "$1")
  default_tags = {
    terraform_git_repo = local.terraform_git_repo
    terraform_base_path = local.terraform_base_path
    created_by = "exNihlio"
    project = "gitlab"
  }
}

resource "aws_vpc" "this" {
  cidr_block = var.vpc_cidr_block
  enable_dns_support = true
  enable_dns_hostnames = true
}

resource "aws_subnet" "this_private" {
  count = 1
  vpc_id = aws_vpc.this.id
  availability_zone = data.aws_availability_zones.azs.names[count.index]
  cidr_block = cidrsubnet(var.vpc_cidr_block, 12, count.index)
  tags = merge(local.default_tags,
    { Name = "private-subnet-${count.index}",
      availability_zone = data.aws_availability_zones.azs.names[count.index]})
}

resource "aws_subnet" "this_public" {
  vpc_id = aws_vpc.this.id
  availability_zone = data.aws_availability_zones.azs.names[0]
  cidr_block = cidrsubnet(var.vpc_cidr_block, 12, 3)
  tags = merge(local.default_tags,
    { Name = "public-subnet-${data.aws_availability_zones.azs.names[0]}",
      availability_zone = data.aws_availability_zones.azs.names[0]})
}

### IGW
resource "aws_internet_gateway" "this" {
  vpc_id = aws_vpc.this.id
}

### NGW
resource "aws_nat_gateway" "this" {
  allocation_id = aws_eip.this.id
  subnet_id = aws_subnet.this_public.id
  depends_on = [aws_internet_gateway.this]
}

### EIP
resource "aws_eip" "this" {
  domain = "vpc"
}

### route tables
resource "aws_route_table" "this_public" {
  vpc_id = aws_vpc.this.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this.id
  }
}
resource "aws_route_table" "this_private" {
  vpc_id = aws_vpc.this.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.this.id
  }
}

### route table associations
resource "aws_route_table_association" "this_public"  {
  subnet_id = aws_subnet.this_public.id
  route_table_id = aws_route_table.this_public.id
}

resource "aws_route_table_association" "this_private" {
  count = 1
  subnet_id = aws_subnet.this_private[count.index].id
  route_table_id = aws_route_table.this_private.id
}
### security groups
resource "aws_security_group" "this_bastion" {
  name = "bastion-sg"
  description = "allow ssh from web"
  vpc_id = aws_vpc.this.id
  ingress {
    description = "allow ssh from web"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description = "allow all egress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "this_gitlab" {
  name = "gitlab-sg"
  description = "Allow connections to GitLab instances"
  vpc_id = aws_vpc.this.id
  ingress {
    description = "allow ssh from bastion"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_groups = [aws_security_group.this_bastion.id]
  }
  ingress {
    description = "allow HTTP from bastion"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_groups = [aws_security_group.this_bastion.id]
  }
  ingress {
    description = "allow HTTPS from bastion"
    from_port = 443
    to_port = 443
    protocol = "tcp"
    security_groups = [aws_security_group.this_bastion.id]
  }
  ingress {
    description = "allow ICMP"
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description = "allow all egress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_security_group" "this_service" {
  name = "gitlab-services"
  description = "allow Postgres connections from GitLab"
  vpc_id = aws_vpc.this.id
  ingress {
    description = "Allow SSH"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_groups = [aws_security_group.this_bastion.id]
  }
  ingress {
    description = "allow Postgres connections"
    from_port = 5432
    to_port = 5432
    protocol = "tcp"
    security_groups = [aws_security_group.this_gitlab.id]
  }
  egress {
    description = "allow all egress"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_instance" "this_gitlab" {
  count = 1
  ami = data.aws_ami.ubuntu_jammy_arm64.id
  subnet_id = aws_subnet.this_private[count.index].id
  instance_type = "t4g.medium"
  user_data = file("cloud-init-gitlab.yml")
  user_data_replace_on_change = true
  vpc_security_group_ids = [aws_security_group.this_gitlab.id]
  tags = merge(local.default_tags, {Name = "gitlab"})
}

resource "aws_instance" "this_service" {
  count = 1
  ami = data.aws_ami.ubuntu_jammy_arm64.id
  subnet_id = aws_subnet.this_private[count.index].id
  instance_type = "t4g.micro"
  user_data = file("cloud-init-gitlab.yml")
  user_data_replace_on_change = true
  vpc_security_group_ids = [aws_security_group.this_service.id]
  tags = merge(local.default_tags, {Name = "services"})
}

resource "aws_instance" "this_bastion" {
  ami = data.aws_ami.ubuntu_jammy_arm64.id
  associate_public_ip_address = true
  subnet_id = aws_subnet.this_public.id
  instance_type = "t4g.micro"
  user_data = file("cloud-init-bastion.yml")
  user_data_replace_on_change = true
  vpc_security_group_ids = [aws_security_group.this_bastion.id]
  tags = merge(local.default_tags,{Name = "gitlab-bastion"})
}

resource "aws_route53_zone" "this" {
  name = "example.com"
  vpc {
    vpc_id = aws_vpc.this.id
  }
}

resource "aws_route53_record" "this" {
  count = 1
  zone_id = aws_route53_zone.this.zone_id
  name = aws_instance.this_gitlab[count.index].tags_all["Name"]
  records = [aws_instance.this_gitlab[count.index].private_ip]
  type = "A"
  ttl = 300
}

resource "aws_route53_record" "this_service" {
  count = 1
  zone_id = aws_route53_zone.this.zone_id
  name = aws_instance.this_service[count.index].tags_all["Name"]
  records = [aws_instance.this_service[count.index].private_ip]
  type = "A"
  ttl = 300
}
