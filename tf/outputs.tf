output "bastion_ip" {
  value = aws_instance.this_bastion.public_ip
}

output "gitlab_host_ip" {
  value = aws_instance.this_gitlab[0].private_ip
}
