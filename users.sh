#!/usr/bin/env bash
# Script to output the username and home directory of
# every user on a UNIX system, with a ":" delimiter added.

# This script was tested and run on an OpenSUSE Leap 15.5 container 
# on RancherOS using GNU bash, version 4.4.23(1)-release (aarch64-suse-linux-gnu)

# This script was written to conform with the with Google Shell Style 
# Guide: https://google.github.io/styleguide/shellguide.html

# By default, most UNIX systems store users in /etc/passwd,
# and passwords in /etc/shadow, but lets add some flexibility;
# also, using variables makes inevitable refactoring easier

# Bash best practices dictate that variables should be all caps
USER_FILE=/etc/passwd

function main() {
  awk -F: '{print $1":"$6}' ${USER_FILE}
}

main "${@}"
